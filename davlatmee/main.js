let box = document.querySelector(".box");
let form = document.forms.add;
let information = [];

form.onsubmit = (e) => {
  e.preventDefault();

  let obj = {
    id: Math.random(),
    isDone: "False",
    time: new Date().getHours() + ":" + new Date().getMinutes(),
  };
  let fm = new FormData(form);

  fm.forEach((value, key) => {
    obj[key] = value;
    if (value.length > 0) {
      information.push(obj);
    }
    upload();
  });
};
function upload() {
  box.innerHTML = "";
  for (let item of information) {
    let item_ = document.createElement("div");
    let item_p = document.createElement("p");
    let item_span = document.createElement("span");
    let hide = document.createElement("img");
    let itemPhide = document.createElement("div");
    let highlighter = document.createElement("img");

    item_.classList.add("item");

    item_p.innerHTML = item.head;
    item_span.innerHTML = item.time;
    hide.src = "./img/free-icon-clear-1632708.png";
    hide.classList.add("img");
    highlighter.src = "./img/highlighter.png";
    highlighter.classList.add("highlighter");

    item_.append(item_p);
    item_p.append(itemPhide);
    item_.append(item_span);
    item_.append(hide);
    box.append(item_);
    item_p.append(highlighter);

    item_p.onclick = () => {
      if (item.isDone == true) {
        item.isDone = false;
        item_p.classList.remove("p_active");
      } else {
        item.isDone = true;
        item_p.classList.add("p_active");
      }
      console.log(item);
    };
    // hide.onclick = () => {
    //   item_.classList.add('hide')
    //   information.slice(item)
    // }
    hide.onclick = () => {
      var g = document.querySelector(".box");
      for (var i = 0, len = g.children.length; i < len; i++) {
        (function (index) {
          g.children[i].onclick = function () {
            item_.classList.add("hide");
            information.splice(index, 1);
          };
        })(i);
      }
    };
  }
}
